package com.company;

public class Main {

    /**
     * In order to test/use this program use following commands:
     * tree.add(data); - to add a node
     * tree.delete(data); - to remove a node
     * tree.search(data); - to find a node
     *
     * BTreePrinter is a class used for easier representation of AVL Tree.
     * print method does exist in AVLTree.java class.
     * @param args
     */
    public static void main(String[] args) {


        AVLTree tree = new AVLTree();
        BTreePrinter print = new BTreePrinter();

        // 5, 4, 7, 1, 2, 8, 9, 12
        tree.add(5);
        tree.add(4);
        tree.add(7);
        tree.add(1);
        tree.add(2);
        tree.add(8);
        tree.add(9);
        tree.add(12);
        tree.add(3);

        tree.delete(5);
        tree.delete(12);
        tree.delete(7);

        tree.add(23);
        print.printNode(tree.root);

        tree.search(1);
        tree.search(800);


    }
}
