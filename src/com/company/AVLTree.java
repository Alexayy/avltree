package com.company;

/**
 * AVL Tree, a data structure that differs from classical Binary Search Tree.
 * Key difference is AVL Tree's ability to self-balance nodes. Balancing is when the tree
 * tries to keep balance factor ∈[-1, 1]. We can get balance factor by substracting height of the right sub tree
 * from height of the left sub tree. In order to preform balancing, there are functions called
 * "ROTATIONS". There are 4 rotations: left rotation, right rotation, left-right rotation and right-left rotation.
 */
class AVLTree {

    TreeNode root;

    /**
     * Recursively adds elements to the tree. This method is forwarded to {@code void add(int data)}
     * to ease insertion in the main(); method.
     * @param node node that will be created
     * @param data data that will be entered
     * @return balanced tree
     */
    private TreeNode insert(TreeNode node, int data) {

        if (node == null)
            return new TreeNode(data);

        if (data < node.getData())
            node.setLeft(insert(node.getLeft(), data));

        else if (data > node.getData())
            node.setRight(insert(node.getRight(), data));

        else
            return node;

        node.setHeight(1 + max(height(node.getLeft()), height(node.getRight())));
        return rotate(node, data);
    }

    /**
     * Gets called in main method.
     * @param data data to be added.
     */
    void add(int data) {
        root = insert(root, data);
    }

    /**
     * Recursively deletes elements from the tree. This method is forwarded to {@code void delete(int data)}
     * to ease deletion from the main(); method.
     * @param node node that will be deleted
     * @param data data that will be deleted with the node
     * @return balanced tree
     */
    public TreeNode remove(TreeNode node, int data) {

        if (node == null)
            return null;

        if (data < node.getData())
            node.setLeft(remove(node.getLeft(), data));

        else if (data > node.getData())
            node.setRight(remove(node.getRight(), data));

        else {

            if (node.getLeft() == null && node.getRight() == null)
                return null;

            if (node.getLeft() == null) {
                TreeNode temp = node.getRight();
                node = null;
                return temp;
            } else if (node.getRight() == null) {
                TreeNode temp = node.getLeft();
                node = null;
                return temp;
            }

            TreeNode temp = predecessor(node.getLeft());
            node.setData(temp.getData());
            node.setLeft(remove(node.getLeft(), temp.getData()));
        }

        if (node == null)
            return null;

        node.setHeight(max(height(node.getLeft()), height(node.getRight())) + 1);
        return rotate(node, data);
    }

    /**
     * Gets called in main method.
     * @param data data to be deleted.
     */
    void delete(int data) {
        root = remove(root, data);
    }

    /**
     * Method that checks balance factor and rotates the tree if balance factor is not in the range.
     * @param node "problematic" node that causes disbalance
     * @param data data in problematic node
     * @return the problematic node on new position
     */
    private TreeNode rotate(TreeNode node, int data) {
        int balanceCount = balance(node);

        if (balanceCount > 1 && data < node.getLeft().getData())
            return rightRotation(node);

        if (balanceCount < -1 && data > node.getRight().getData())
            return leftRotation(node);

        if (balanceCount > 1 && data > node.getLeft().getData()) {
            node.setLeft(leftRotation(node.getLeft()));
            return rightRotation(node);
        }

        if (balanceCount < -1 && data < node.getLeft().getData()) {
            node.setRight(rightRotation(node.getRight()));
            return leftRotation(node);
        }

        return node;
    }

    /**
     * Recursively searches elements from the tree. This method is forwarded to {@code void search(int data)}
     * to ease searching from the main(); method.
     * @param node node that will be searched for with the data
     * @param data data that will be searched for
     * @return node with the value
     */
    private TreeNode seek(TreeNode node, int data) {

        if (node == null)
            return null;

        else if (data == node.getData()) {
            System.out.println("Found value: " + node.getData());
            return node;
        } else if (data > node.getData())
            return seek(node.getRight(), data);

        else
            return seek(node.getLeft(), data);
    }

    /**
     * Gets called in main method. Since {@code private TreeNode seek(TreeNode node, int data) } returns the node,
     * root can be assigned a new value in form of a recursive method.
     * @param data data to be searched.
     */
    void search(int data) {
        root = seek(root, data);
    }

    /**
     * Method that preforms right rotation.
     * @param node "problematic" node that causes disbalance
     * @return the parent node
     */
    private TreeNode rightRotation(TreeNode node) {
        TreeNode parentNode = node.getLeft();
        TreeNode temp = parentNode.getRight();

        parentNode.setRight(node);
        node.setLeft(temp);

        updateHeight(node, parentNode);

        return parentNode;
    }

    /**
     * Method that preforms left rotation.
     * @param node "problematic" node that causes disbalance
     * @return the parent node
     */
    private TreeNode leftRotation(TreeNode node) {
        TreeNode parent = node.getRight();
        TreeNode temp = parent.getLeft();

        parent.setLeft(node);
        node.setRight(temp);

        updateHeight(node, parent);

        return parent;
    }

    /**
     * Method that updates height of the tree.
     * @param nodeX left node
     * @param nodeY right node
     */
    private void updateHeight(TreeNode nodeX, TreeNode nodeY) {
        nodeX.setHeight(max(height(nodeX.getLeft()), height(nodeX.getRight())) + 1);
        nodeY.setHeight(max(height(nodeY.getLeft()), height(nodeY.getRight())) + 1);
    }

    /**
     * Method that returns current height
     * @param node
     * @return height
     */
    private int height(TreeNode node) {
        if (node == null)
            return 0;

        return node.getHeight();
    }

    /**
     * A method that returns a higher value.
     * @param a height of the left subtree
     * @param b height of the right subtree
     * @return higher value
     */
    private int max(int a, int b) {
        return (a > b) ? a : b;
    }

    /**
     * A method that returns a predecessor node.
     * @param node
     * @return predecessor node
     */
    private TreeNode predecessor(TreeNode node) {
        TreeNode current = node;

        while (current.getRight() != null)
            current = current.getRight();

        return current;
    }

    /**
     * Method that returns current balance factor
     * @param node
     * @return balance factor
     */
    private int balance(TreeNode node) {
        if (node == null)
            return 0;

        return height(node.getLeft()) - height(node.getRight());
    }

    /**
     * A method that prints the AVL tree.
     * @param tree to be printed
     */
    public void printTree(TreeNode tree) {
        if (tree.getLeft() != null)
            printTree(tree.getLeft());

        System.out.println(tree.getData());

        if (tree.getRight() != null)
            printTree(tree.getRight());
    }

    /**
     * Gets called in main method to ease the printing.
     */
    public void printTree() {
        printTree(root);
    }
}
